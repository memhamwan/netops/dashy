# Dashy on MemHamWAN

This repository tracks the configuration and deployment of [Dashy](https://github.com/Lissy93/dashy).

## Why?

GitLab offers a nice integration with our kubernetes cluster to track and monitor the deployment, as well as offer configuration-as-code. This repository therefore just contains the kubernetes configurations and deployment automation, and then use the "environments" feature in Gitlab to see the deployment progress.

## Contributing

Just make changes to k8s-objects in a branch. In the near future, we'll have "review environments" setup so that you can check your work without going to production. For now though, the CI will run a kubelint to make sure that the deployment doesn't get broken, and you're on your own for making sure that the dashy configuration itself is still good.
